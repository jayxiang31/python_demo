# -*- utf-8 -*-
"""
@url: https://blog.csdn.net/u014534808
@Author: 码农飞哥
@File: fifth_15_test.py
@Time: 2021/11/30 22:12
@Desc:
"""
import io

# 2.1.  读取文件有几种方式？分别有啥不同
# 3种方式，分别是
f = open('io.txt', encoding='utf-8')
# 读取所有的数据
print(f.read())
# 读取单行数据
f = open('io.txt', encoding='utf-8')
print(f.readline())
# 按行读取数据，数据读取到一个列表中
f = open('io.txt', encoding='utf-8')
print(f.readlines())

# 2.2.  写文件有几种方式？分别有啥不同
f = open('write.txt', mode='w', encoding='utf-8')
for i in range(2):
    f.write('码农飞哥' + str(i))

# 其中，file表示已经打开的文件对象，str表示要写入文件的内容，可以支持多行数据的写入。
f = open('write.txt', encoding='utf-8')
file = open('write_file.txt', mode='w', encoding='utf-8')
file2 = open("io.txt", encoding='utf-8')
file.writelines(file2.read())
file.close()

# 2.3. 新建一个文件 feige.txt 。 向文件中写入 打卡学习，我定能成功 。然后在读取出来。
with open('feige.txt', mode='w', encoding='utf-8') as f:
    f.write('打卡学习，我定能成功')

with open('feige.txt', encoding='utf-8') as f:
    print(f.read())
