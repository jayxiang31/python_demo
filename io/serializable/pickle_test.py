#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Pickle.py
@date: 2021/1/4 15:10
@desc: 
"""
import pickle


d =dict(name='Bob', age=20, score=88)
# 序列化(把任意对象序列化成一个bytes，
# 然后，就可以把这个bytes写入文件)
print(pickle.dumps(d))
# 写入文件
try:
    f = open('dump.txt', 'wb')
    pickle.dump(d, f)
finally:
    f.close()
# 反序列化
try:
    f = open('dump.txt', 'rb')
    d = pickle.load(f)
    print(d)
finally:
    f.close()
