#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: json_test.py
@date: 2021/1/4 15:20
@desc: 
"""
import json

# d = dict(name='Bob', age=20, score=88)
# print(json.dumps(d))
# json_str = '{"name": "Bob", "age": 20, "score": 88}'
# print(json.loads(json_str))



class Student(object):
    def __init__(self, name, score):
        self.name = name
        self.score = score


def json2object(d):
    className = d.pop('__class__', None)
    if className:
        cls = globals()[className]
        return cls(**d)
    return d


def student2dict(std):
    return {
        'name': std.name,
        'score': std.score
    }

s = Student(name='Bob', score=95)
str = json.dumps(s, default=student2dict)
print(str)

text = '''
{
  "__class__": "Student",
  "name": "Bob",
  "score": 95
}
'''

s = json.loads(text, object_hook=json2object)

print(s.name)
print(s.score)

obj = dict(name='小明', age=20)
s = json.dumps(obj, ensure_ascii=False)
print(s)
