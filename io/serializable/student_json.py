#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Student.py
@date: 2020/12/30 15:45
@desc: 
"""


class Student(object):
    def __init__(self, name, score):
        self.name = name
        self.score = score

    def print_score(self):
        print('%s:%s' % (self.name, self.score))

    def student2dict(std):
        return {
            'name': std.name,
            'score': std.score
        }

    def dict2student(self):
        return Student(self['name'], self['score'])

