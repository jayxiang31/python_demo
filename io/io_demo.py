#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: io_demo.py
@date: 2021/1/4 11:35
@desc: 
"""
# 打开文件
f = open("C:\\Users\\码农飞哥\\Desktop\\file文件读写.txt", 'r')
# 读取文件
print(f.read())
# 关闭文件流
f.close()

# 保证正确的读文件并关闭文件流
try:
    f = open("C:\\Users\\码农飞哥\\Desktop\\file文件读写.txt", 'r')
    print(f.read())
finally:
    if f:
        f.close()
    # 等价于
with open("C:\\Users\\码农飞哥\\Desktop\\file文件读写.txt", 'r') as  f:
    print(f.read())
