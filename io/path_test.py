"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: path_test.py
@date: 2021/8/9 21:01
@desc: 
"""
import os

# 获取绝对路径
print(os.path.abspath("."))
# 判断是否是绝对路径
path = '/Volumes/development'
print(os.path.isabs(path))
# 将返回从 start 路径到 path 的相对路径的字符串。
# 如果没有提供 start，就使用当前工作目录作为开始路径。
print(os.path.relpath("/Python_learn", "/Volumes"))
print(os.path.relpath('/Python_learn'))
# 获取当前类的路径
path = "/Volumes/development/Python_learn/PycharmProjects/python_demo_1/demo/io/path_test.py"
print(os.path.basename(path))
print(os.path.dirname(path))
print(os.path.split(path))


# 判断路径是否存在
print(os.path.exists("/Volumes"))
print(os.path.isdir('/Volumes/development'))
print(os.path.isfile('/Volumes/development/Python_learn/PycharmProjects/python_demo_1/demo/io/path_test.py'))
