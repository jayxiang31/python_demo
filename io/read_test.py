"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: read_test.py
@date: 2021/8/9 22:24
@desc: 
"""
import os

f = open('my_file.txt')
print(f.read())
f.close()

f = open('my_file.txt', 'rb')
f_bytes = f.read()
print(f_bytes.decode('utf-8'))
f.close()

f = open('my_file.txt')
line_txt = f.readline()
while line_txt:
    print(line_txt)
    line_txt = f.readline()
f.close()

f = open('my_file.txt')
print(f.readline(5))
f.close()

f = open('my_file.txt')
print(f.readlines())

