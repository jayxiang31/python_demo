#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: os_test.py
@date: 2021/1/4 14:00
@desc: 
"""
import os

# 获取系统类型
print(os.name)
# 获取系统环境变量
print(os.environ)
# 查看当前目录的绝对路径
print(os.path.abspath('.'))
# 在某个目录下创建一个新目录，首先把新目录的完整地址表示出来
print(os.path.join("d:/", 'test'))
# 创建目录
print(os.mkdir("d:/test"))
# 删除一个目录
print(os.rmdir('d:/test'))
# 获取文件扩展名
print(os.path.splitext(os.path.abspath('.') + '/file文件读写.txt'))
# TODO 对文件重命名
# os.rename(os.path.abspath('.') + '/file文件读写.txt', os.path.abspath('.') + '/file.txt')
# 列出当前目录下的所有目录
gen = [x for x in os.listdir('.') if os.path.isdir(x)]
print(gen)
# 要列出所有的.py文件，也只需一行代码：
list = [x for x in os.listdir('.') if os.path.isfile(x) and os.path.splitext(x)[1] == '.py']
print(list)
