#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: dir_test.py
@date: 2021/1/4 14:06
@desc: 
"""
import os

# 利用os模块编写一个能实现dir -l输出的程序。
gen = [x for x in os.listdir('D:\\学习记录')]
print(gen)


# 编写一个程序，能在当前目录以及当前目录的所有子目录下查找文件名包含指定字符串的文件，并打印出相对路径。
def gci(filepath):
    files = os.listdir(filepath)
    for fi in files:
        fi_d = os.path.join(filepath, fi)
        if os.path.isdir(fi_d):
            gci(fi_d)
        elif os.path.splitext(fi_d)[1] == '.py':
            print(fi_d)
gci(".")