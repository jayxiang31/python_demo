#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: stringio_test.py
@date: 2021/1/4 13:47
@desc: 
"""
from io import StringIO

# 写入StringIO,类似于Java中的BufferedWriter
f = StringIO()
f.write('hello')
f.write(' ')
f.write('world!')
print(f.getvalue())
# 读取StringIO,类似于BufferedReader
f = StringIO('Hello!\nHi\nGoodbye!')
while True:
    s = f.readline()
    if s == '':
        break
    print(s.strip())
