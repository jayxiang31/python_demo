#-*- utf-8 -*-
"""
@url: https://blog.csdn.net/u014534808
@Author: 码农飞哥
@File: uiautomation_test.py
@Time: 2021/11/17 20:23
"""
import uiautomation as auto
auto.uiautomation.SetGlobalSearchTimeout(1)

friendWindow = auto.WindowControl(
    searchDepth=1, Name="朋友圈", ClassName='SnsWnd')
friendWindow.SetActive()
item = friendWindow.ListItemControl(searchDepth=6, searchInterval=0.01)
comment_button = item.ButtonControl(Name="评论")
comment_pane = comment_button.GetParentControl().GetNextSiblingControl()
like_pane = comment_pane.GetFirstChildControl()
nicknames = []
for control, depth in auto.WalkControl(like_pane, maxDepth=4):
    if not control.Name or depth != 4 or not isinstance(control, auto.ButtonControl):
        continue
    nicknames.append(control.Name)
print(nicknames)
