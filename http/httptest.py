#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: httptest.py
@date: 2021/2/4 19:25
@desc: 
"""
import json
import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder


def getToken():
    url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxb80065402226b5ee&secret=67d827fdc80348148e0e6eebf4c53d96"
    # 请求接口
    res = requests.get(url)
    # 对返回的内容进行编码
    content = res.content.decode('utf-8')
    # 将json字符串反序列化
    tokenJson = json.loads(content)
    print(tokenJson)


# application/x-www-form-urlencoded
def post_form_urlencoded():
    url = "http://127.0.0.1:5041/exp-h5/v1/ls/voice/save"
    data = {"dst_audio_url": "https://test.ai.changyan.com/test/9d62ca66-326f-4070-9993-5f89f790dc68.wav"}
    res = requests.post(url, data)
    content = res.content.decode('utf-8')
    print(content)


def post_json():
    url = "http://127.0.0.1:5041/exp-h5/v1/ls/json"
    x_header = {
        'Content-Type': 'application/json; charset=utf-8',
    }
    body = json.dumps({"name": "张三", "age": 12})
    res = requests.post(url, data=body, headers=x_header, verify=False, timeout=(30, 30))
    content = res.content.decode('utf-8')
    print(content)


def post_form_data():
    post_url = "http://127.0.0.1:5041/exp-h5/v1/ls/upload"
    headers = {}
    multipart_encoder = MultipartEncoder(
        fields={
            'file': (open('D:\\test.txt', 'rb'), 'application/octet-stream'),
            'file_name': '张三'
        }
    )
    headers['Content-Type'] = multipart_encoder.content_type
    post_response = requests.post(post_url, data=multipart_encoder, headers=headers, verify=False)
    content = post_response.content.decode('utf-8')
    print(content)


if __name__ == '__main__':
    # getToken()
    post_form_urlencoded()
    post_json()
    post_form_data()
