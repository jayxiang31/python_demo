#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: fact_fun.py
@date: 2020/12/29 10:41
@desc: 
"""


def fact(n):
    if n == 1:
        return 1
    return n * fact(n - 1)
