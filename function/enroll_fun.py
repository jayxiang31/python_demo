#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: enroll_fun.py
@date: 2020/12/28 17:22
@desc: 
"""


def enroll(name, gender, age=6, city='Beijing'):
    print('name:', name)
    print("gender:", gender)
    print("age:", age)
    print("city:", city)


print(enroll('张三', '一年级'))
print('************************** ')
print(enroll('李四', '二年级', 7))
