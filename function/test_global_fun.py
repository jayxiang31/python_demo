name = "张三"


def test_1():
    name = "李四"
    print('局部变量name=', name)
    # 访问全局变量
    print('全局变量name=', globals()['name'])


test_1()

name = "码农飞哥"
def test_2():
    global name
    # 访问全局变量
    print('全局变量name=', name)
    name = "小伟伟"
    print('局部变量name=', name)
test_2()
