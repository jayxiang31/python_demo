# 计算一个数的n次幂
def nth_power(exponent):
    def exponent_of(base):
        return base ** exponent

    return exponent_of


# 计算一个数的平方
square = nth_power(2)
# 计算一个数的立方
cube = nth_power(3)

print(square(2))
print(cube(2))
