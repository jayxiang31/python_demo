def param_test(obj):
    obj += obj
    print('形参值为:', obj)


print('*******值传递*****')
a = '码农飞哥'
print('a的值为：', a)
param_test(a)
print('实参的值为:', a)

print("*******引用传递*****")
b = [1, 2, 3]
print('b的值为：', b)
param_test(b)
print('实参的值为：', b)


def param_test(name, age):
    print('name=', name)
    print('age=', age)


data = ['码农飞哥', 18]
param_test(*data)
