#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: calc_fun.py
@date: 2020/12/29 10:09
@desc: 
"""


def calc(*numbers):
    sum = 0
    for n in numbers:
        sum = sum + n
    return sum


print(calc(10, 9))


# 以下函数允许计算两个数的乘积，请稍加改造，
# 变成可接收一个或多个数并计算乘积：
def product(x, *args):
    mul = x
    for n in args:
        mul = mul * n
    return mul


def record(str, **kwargs):
    print('str=', str)
    print('kwargs=', kwargs)


record('测试', name='码农飞哥', age=20)
record('测试2')
