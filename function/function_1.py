#!/usr/bin/env python
# coding=utf-8
from demo.function.abstest import my_abs
from demo.function.multi_return_fun import move
import math
from demo.function.power_fun import power
from demo.function.enroll_fun import enroll
from demo.function.calc_fun import calc
from demo.function.fact_fun import fact
from demo.function.fact_fun2 import fact2
from demo.function.hanno_fun import hanno_move

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: function_1.py
@date: 2020/12/28 16:14
@desc:
http://docs.python.org/3/library/functions.html#abs
"""
# 取绝对值的函数
print(abs(-20))
print(max(1, 3, 12))
print(max(2, 3, -5, 1))
# 数据类型转换
int('123')
int(12.34)
float('12.34')
str(1.23)
bool(1)
bool('')

# 绝对值函数
print(my_abs(-23))
# print(my_abs('a'))
# 返回多个值得函数
x, y = move(100, 100, 60, math.pi / 6)
print(x, y)
# 求平方
print(power(5, 3))
print(power(5))
# 学生注册
print(enroll('张三', "F"))
# 可变参数计算
print(calc(1, 2, 3))
# 递归函数
print(fact(1))
print(fact(5))
# 递归函数优化（尾递归优化）
print(fact2(1))
print(fact2(5))
# 汉诺塔的实现
print(hanno_move(3, 'A', 'B', 'C'))
