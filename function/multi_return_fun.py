#!/usr/bin/env python
# coding=utf-8
import math

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: multi_return_fun.py
@date: 2020/12/28 16:37
@desc: 
"""


def move(x, y, step, angle=0):
    nx = x + step * math.cos(angle)
    ny = y - step * math.sin(angle)
    return nx, ny


def multi_return():
    return_tuple = ('张三', 12)
    return return_tuple


def multi_return2():
    return '张三', 12


print(multi_return())

result = multi_return2()
print('multi_return2返回值是=,类型是=', result, type(result))
