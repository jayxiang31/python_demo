def add(x, y):
    return x + y


print(add(2, 3))

# 使用lambda表达式
add = lambda x, y: x + y
print(add(2, 3))

test_list = [1, 2, 3, 4]
new_list = map(lambda x: x * 3, test_list)
print(list(new_list))

test_list1 = [1, 3, 5]
test_list2 = [2, 4, 6]
new_list = map(lambda x, y: x + y, test_list1, test_list2)
print(list(new_list))

test_list3 = [1, 2, 3, 4, 5]
new_list = filter(lambda x: x % 2 == 0, test_list3)
print(list(new_list))

import functools

test_list4 = [1, 2, 3, 4, 5]
product = functools.reduce(lambda x, y: x + y, test_list4)
print(product)
