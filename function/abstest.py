#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: my_abs.py
@date: 2020/12/28 16:28
@desc: 
"""

# 返回绝对值函数
def my_abs(x):
    """
    返回绝对值
    :param x:
    :return:
    """
    if not isinstance(x, (int, float)):
        raise TypeError('传入的数据类型不对')
    if x >= 0:
        return x
    else:
        return -x


if __name__ == '__main__':
    x = my_abs(-3)
    print(x)
    print(my_abs.__doc__)
