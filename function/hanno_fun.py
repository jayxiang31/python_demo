#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: hanno_fun.py
@date: 2020/12/29 14:30
@desc:
"""


# 请编写move(n, a, b, c)函数，它接收参数n，
# 表示3个柱子A、B、C中第1个柱子A的盘子数量，然后打印出把所有盘子从A借助B移动到C的方法
def hanno_move(n, a, b, c):
    if n == 1:
        print(a, '---->', c)
    else:
        hanno_move(n - 1, a, c, b)
        hanno_move(1, a, b, c)
        hanno_move(n - 1, b, a, c)
