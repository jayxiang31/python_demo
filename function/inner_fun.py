# 全局函数
def outdef():
    # 局部函数
    def indef():
        print("调用局部函数")

    # 调用局部函数
    return indef


# 调用全局函数
new_indef = outdef()
# 调用全局函数中的局部函数
new_indef()


# 全局函数
def outer_fun():
    name = '码农飞哥'

    def inner_fun():
        print('调用局部函数')
        nonlocal name
        print('全局变量name=', name)
        name = '小伟伟'
        print('局部变量name=', name)

    return inner_fun

# 调用局部函数
new_inner_fun = outer_fun()
new_inner_fun()


def my_fun():
    print("正在执行my_fun函数")


# 将函数赋值给其他变量
other = my_fun
# 间接调用my_fun()函数
other()


def my_fun1():
    return '加油，你一定行'


def my_fun2(name, my_fun1):
    return name + my_fun1


print(my_fun2('码农飞哥', my_fun1()))
