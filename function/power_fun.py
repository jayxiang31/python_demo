#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: power_fun.py
@date: 2020/12/28 17:08
@desc: 
"""


# 计算x平方，立方等,其中2是默认参数，可以不传
def power(x, n=2):
    s = 1
    while n > 0:
        n = n - 1
        s = s * x
    return s
