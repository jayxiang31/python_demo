def girth(width, height):
    return 2 * width + height


# 调用函数时，必须传递 2 个参数，否则会引发错误
# print(girth(3))
print(girth(2, 3))


def print_info(name, age):
    print('姓名=' + name + " 年龄=" + str(age))


print_info(age=18,name='码农飞哥')
