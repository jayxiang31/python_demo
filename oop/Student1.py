#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Student1.py
@date: 2020/12/30 16:16
@desc: 
"""


class Student1(object):
    def __init__(self, name, score):
        self.__name = name
        self.__score = score

    def print_score(self):
        print('%s:%s' % (self.__name, self.__score))

    def get_name(self):
        return self.__name

    def get_score(self):
        return self.__score

    def set__name(self, name):
        self.__name = name

    def set__score(self, score):
        self.__score = score
