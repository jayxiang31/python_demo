class Rect:
    def __init__(self, area):
        self.__area = area

    @property
    def area(self):
        return self.__area


rect = Rect(30)
print('矩形的面积是:', rect.area)
