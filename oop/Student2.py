#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Student2.py
@date: 2020/12/30 16:38
@desc:

    #
    # def __init__(self, name, gender):
    #     self.name = name
    #     self.__gender = gender

"""


class Student2(object):
    def __init__(self):
        pass

    def get_gender(self):
        return self.__gender

    def set_gender(self, gender):
        self.__gender = gender

    # 定义私有方法
    def __display(self):
        print(self.__gender)


student = Student2()
student.set_gender('男')
student.__display()
