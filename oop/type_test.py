#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: type_test.py
@date: 2020/12/30 17:13
@desc: 
"""
from demo.oop.subandson.Animal import Animal
from demo.oop.subandson.Animal import Dog
from demo.oop.subandson.Animal import Cat

# 判断对象类型
print(type(123) == type(456))
print(type(123) == int)
print(type('abc') == type('123'))
print(type('abc') == str)
print(type('abc') == type(123))
# 使用isinstance()
a = Animal()
d = Dog()
c = Cat()
print(isinstance(a, Animal))
print(isinstance(d, Animal))
print(isinstance(d, Dog))
print(isinstance(d, Cat))
# 使用dir()
print(dir('ABC'))
