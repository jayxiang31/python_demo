class Bird:
    #  鸟有翅膀
    def isWing(self):
        print("鸟有翅膀")

    # 鸟会飞
    def fly(self):
        print("鸟会飞")


class Ostrich(Bird):
    # 重写Bird类的fly()方法
    def fly(self):
        print("鸵鸟不会飞")


# 创建Ostrich对象
ostrich = Ostrich()
# 调用Ostrich类中重写的fly()方法
ostrich.fly()

# 调用被重写的方法
Bird.fly(ostrich)
