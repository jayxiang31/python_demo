#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Animal.py
@date: 2020/12/30 16:46
@desc: 
"""


class Animal(object):
    def run(self):
        print('动物奔跑')
class Dog(Animal):
    def run(self):
        print('小狗奔跑')
class Cat(Animal):
    def run(self):
        print('小猫奔跑')


animal=Animal()
animal.run()

animal = Dog()
animal.run()

animal = Cat()
animal.run()
