#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Animal.py
@date: 2020/12/30 16:46
@desc: 通过给WhoRun类中的say()函数添加一个who参数
"""


class WhoRun:
    def run(self, who):
        who.run()


class Animal(object):
    def run(self):
        print('动物奔跑')


class Dog(Animal):
    def run(self):
        print('小狗奔跑')


class Cat(Animal):
    def run(self):
        print('小猫奔跑')


a = WhoRun()
a.run(Animal())
a.run(Dog())
a.run(Cat())
