class Animal:
    def __init__(self, food):
        self.food = food


class People(Animal):
    def __init__(self, name, food):
        self.name = name
        super().__init__(food)

    def say(self):
        print("我是人，名字是：{0},吃的食物是：{1}".format(self.name, self.food))


people = People('张三', '熟食')
people.say()
