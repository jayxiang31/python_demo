# class TypeTest:
#     def say(self):
#         print('我要学Python!')

def say(self):
    print('我要学Python!')


# 使用type()函数创建类
TypeTest = type("TypeTest", (object,), dict(say=say, name="码农飞哥"))
# 创建一个TypeTest实例对象
typeTest = TypeTest()
typeTest.say()
print(typeTest.name)
