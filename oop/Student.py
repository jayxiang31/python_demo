#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Student.py
@date: 2020/12/30 15:45
@desc: 
"""


# 这是单行注释
class Student(object):
    def __init__(self, name, score):
        self.name = name
        self.score = score

    @property
    def serialize(self):
        return {"name": self.name, "score": self.score}


student = Student('张三', 100)
print(student.serialize)
