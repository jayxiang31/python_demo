#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Student_test.py
@date: 2020/12/30 15:47
@desc: 
"""
from oop.Student import Student

bart = Student('Bart Simpson', 59)
lisa = Student('Lisa Simpson', 87)
bart.print_score()
lisa.print_score()
print(bart.name)

from oop.Student1 import Student1

bart1 = Student1('码农', 59)
lisa1 = Student1('飞哥', 80)
bart1.print_score()
lisa1.print_score()

bart = Student('Bart', 'male')
if bart.get_gender() != 'male':
    print('测试失败!')
else:
    bart.set_gender('female')
    if bart.get_gender() != 'female':
        print('测试失败!')
    else:
        print('测试成功!')
