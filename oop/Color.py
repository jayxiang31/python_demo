from enum import Enum


class Color(Enum):
    # 为序列值制定value值
    red = 1
    green = 2
    blue = 3


# 获取枚举类成员的3种方式
print(Color.red)
print(Color['red'])
print(Color(1))
# 调取枚举类成员的value和name
print(Color.red.value)
print(Color.red.name)

# 遍历枚举类中所有成员的2种方式
for color in Color:
    print(color)
