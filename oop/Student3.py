#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Student3.py
@date: 2020/12/30 17:40
@desc: 
"""


class Student3(object):
    count = 0

    def __init__(self, name):
        self.name = name
        Student3.count = Student3.count + 1
