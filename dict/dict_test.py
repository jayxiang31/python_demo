#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: dict_test.py
@date: 2021/4/7 10:28
@desc: 
"""
d = {
    'Michael': 95,
    'Bob': 75,
    'Tracy': 85
}
print('d[\'Michael\'] =', d['Michael'])
print('d[\'Bob\'] =', d['Bob'])
print('d[\'Tracy\'] =', d['Tracy'])
print('d.get(\'Thomas\', -1) =', d.get('Thomas', -1))

plate_info = {
    'coor': {'up_left': [
        358,
        592
    ], 'down_left': 222, 'up_right': 333, 'down_right': 444},
    'plate_type': '蓝牌',
    'e2e': {'e2e_plate_result': '皖AQ9094', 'e2e_plate_conf': 0.9845612815448216},
    'segmentplate': {'seg_plate': '皖AQ9094', 'seg_conf': 0.9974016121455601}, 'plate_id': 1}
print('plate_info[\'plate_info\']=', plate_info)
print('plate_info[\'coor\']=', plate_info['coor'])
coor = plate_info['coor']
# up_left = '(' + str(','.join(coor['up_left'])) + ')'
print('plate_info[\'down_left\']=', str(coor['down_left']))

tuple = (358, 372)
tuple_str = [str(x) for x in tuple]
print(",".join(tuple_str))
