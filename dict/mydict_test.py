#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: mydict_test.py
@date: 2021/1/4 10:56
@desc: 
"""
import unittest
from demo.dict.mydict import Dict


class TestDict(unittest.TestCase):
    def test_init(self):
        a = 1,
        b = 'test'
        d = Dict(a, b)
        self.assertEqual(d, a, 1)
        self.assertEqual(d, b, 'test')
        self.assertTrue(isinstance(d, dict))

    def test_key(self):
        d = Dict()
        d['key'] = 'value'
        self.assertEqual(d.key, 'value')

    def test_attr(self):
        d = Dict()
        d.key = 'value'
        self.assertTrue('key' in d)
        self.assertEqual(d['key'], 'value')

    def test_keyerror(self):
        d = Dict()
        with self.assertRaises(KeyError):
            value = d['empty']

    def test_attrerror(self):
        d = Dict()
        with self.assertRaises(AttributeError):
            value = d.empty
