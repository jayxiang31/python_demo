#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: student.py
@date: 2021/1/4 11:15
@desc: 
"""


class Student(object):
    def __init__(self, name, score):
        self.name = name
        self.score = score

    def get_grade(self):
        if self.score >= 80 and self.score <= 100:
            return 'A'
        if self.score >= 60 and self.score < 80:
            return 'B'
        elif self.score >= 0 and self.score < 80:
            return 'C'
        else:
            raise ValueError
