"""
@author: 码农飞哥
@file: process.py
@date: 2021/9/12 11:52
@desc:
提供了Pool()函数，专门用来创建一个进程池
"""

import os
from multiprocessing import Pool
import time


# 定义要调用的方法
def async_fun(add):
    time.sleep(1)
    print("进程号：" + str(os.getpid()) + " " + add)


if __name__ == '__main__':
    add = "码农飞哥,今天是宅家的一天,30岁了还没对象焦虑呀"
    # 创建包含4个进程的进程池
    pool = Pool(processes=4)
    # 提交action
    pool.apply_async(func=async_fun, args=(add,))
    pool.apply_async(func=async_fun, args=("加油加油",))
    pool.close()
    pool.join()
