"""
@author: 码农飞哥
@file: process.py
@date: 2021/9/12 11:52
@desc: 
"""

from multiprocessing import Process
import os


# 定义要调用的方法
def async_fun(name, add):
    for arc in add:
        print(name + str(os.getpid()) + " " + arc)


if __name__ == '__main__':
    my_tuple = ("码农飞哥", "今天是宅家的一天", "30岁了还没对象焦虑呀")

    # 创建进程
    process = Process(target=async_fun, args=("子进程", my_tuple))
    # 启动子进程
    process.start()
    # 启动主进程
    async_fun("主进程", my_tuple)
