"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: executor_map_test.py
@date: 2021/8/29 11:59
@desc: 
"""
from multiprocessing import Pool
import os
import time


def async_add(max):
    time.sleep(1)
    print("进程号：" + str(os.getpid()) + "最大值是" + str(max))


if __name__ == '__main__':
    with Pool(processes=4) as pool:
        # 使用线程池执行max计算
        results = pool.map(async_add, (20, 30, 40, 50))
