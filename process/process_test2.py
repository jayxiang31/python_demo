"""
@author: 码农飞哥
@file: process.py
@date: 2021/9/12 11:52
@desc: 
"""

import multiprocessing
import os


# 定义要调用的方法
def async_fun(name, add):
    for arc in add:
        print(name + str(os.getpid()) + " " + arc)


class MyProcess(multiprocessing.Process):
    def __init__(self, name, add):
        multiprocessing.Process.__init__(self)
        self.add = add
        self.name = name
        # 重写run()方法

    def run(self):
        async_fun(self.name, self.add)


if __name__ == '__main__':
    my_tuple = ("码农飞哥", "今天是宅家的一天", "宅家也不能虚度")
    myprocess = MyProcess("子进程", my_tuple)
    myprocess.start()
    # 主进程
    async_fun("主进程", my_tuple)
