#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Range_fun.py
@date: 2020/12/30 10:32
@desc: 
"""
L = []
for x in range(1, 11):
    L.append(x * x)
print(L)
# 输出乘积
L = [x * x for x in range(1, 11)]
print(L)
# 输出偶数
print([x for x in range(1, 11) if x % 2 == 0])
# if...else if写在表达式
print([x if x % 2 == 0 else -x for x in range(1, 11)])
# 跳过非字符串
L1 = ['Hello', 'World', 18, 'Apple', None]
L2 = [x.lower() for x in L1 if isinstance(x, str)]
print(L2)

d_list = [(x, y) for x in range(5) for y in range(4)]
print(d_list)

# 其表达式相当于
dd_list = []
for x in range(5):
    for y in range(4):
        dd_list.append((x, y))
print(dd_list)

d_tuple = (x * x for x in range(1, 11))
print(tuple(d_tuple))

key_list = ['姓名:码农飞哥', '年龄:18', '爱好:写博客']
test_dict = {key.split(':')[0]: key.split(':')[1] for key in key_list}
print(test_dict)
