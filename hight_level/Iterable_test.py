#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Iterable_test.py
@date: 2020/12/30 14:36
@desc: 
"""
from collections.abc import Iterable

print(isinstance([], Iterable))
print(isinstance({}, Iterable))
print(isinstance('abc', Iterable))
print(isinstance((x for x in range(10)), Iterable))
print(isinstance(100, Iterable))

# 判断是不是迭代器
from collections.abc import Iterator

print(isinstance([], Iterator))
print(isinstance({}, Iterator))
print(isinstance('abc', Iterator))

for x in [1, 2, 3, 4, 5]:
    print('第一个遍历'+str(x))
    pass
# 完全等价于
it = iter([1, 2, 3, 4, 5])
while True:
    try:
        # 获取下一个值
        x = next(it)
        print('第二个遍历'+str(x))
    except StopIteration:
        break
