#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Iterable_fun.py
@date: 2020/12/30 9:45
@desc: 
"""
# 字符串迭代
for ch in 'ABD':
    print(ch)
# 判断是否可迭代
from collections import Iterable

print(isinstance('abc', Iterable))
print(isinstance([1, 2, 3], Iterable))
print(isinstance(123, Iterable))
# 把list变成索引-元素对
for i, value in enumerate(['A', 'B', 'C']):
    print(i, value)


def findMinAndMax(L):
    if len(L) == 0:
        return (None, None)
    else:
        (min, max) = (L[0], L[0])
    for i, value in L:
        if value < min:
            min = value
        elif value > max:
            max = value
    return (min, max)


if findMinAndMax([]) != (None, None):
    print('测试失败!')
elif findMinAndMax([7]) != (7, 7):
    print('测试失败!')
elif findMinAndMax([7, 1]) != (1, 7):
    print('测试失败!')
elif findMinAndMax([7, 1, 3, 9, 5]) != (1, 9):
    print('测试失败!')
else:
    print('测试成功!')

