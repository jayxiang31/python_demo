#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: generator.py
@date: 2020/12/30 10:55
@desc: 
"""
L = [x * x for x in range(10)]
print(L)
# generator生成器
g = (x * x for x in range(10))
for n in g:
    print(n)


def fib(max):
    n, a, b = 0, 0, 1
    while n < max:
        print(b)
        a, b = b, a + b
        n = n + 1
    return 'done'


print(fib(10))


def fib1(max):
    if max == 0 or max == 1:
        return max
    else:
        return fib1(max - 1) + fib1(max - 2)


print(fib1(10))


# 斐波那契变形
# g = fib(6)
# while True:
#     try:
#         x = next(g)
#         print('g:', x)
#     except StopIteration as e:
#         print('Generator return value:', e.value)
#         break


# 遇到yield语句返回，再次执行时从上次返回的yield语句处继续执行。
def odd():
    print('step 1')
    yield 1
    print('step 2')
    yield (3)
    print('step 3')
    yield (5)


o = odd()
next(o)
next(o)
next(o)


# 杨辉三角
def triangles():
    L = [1]
    while True:
        yield L
        L = [L[i] + L[i + 1] for i in range(len(L) - 1)]  # 先生成除首位的数字
        L.insert(0, 1)  # list开头添加 1
        L.append(1)  # list结尾添加 1


print(triangles())


def triangles1():
    l = [1]
    while True:
        yield (l)
        tmp = []
        for k, v in enumerate(l):
            if k == 0:
                tmp.append(1)
            else:
                tmp.append(v + l[k - 1])
            if k == (len(l) - 1):
                tmp.append(1)
        l = tmp
