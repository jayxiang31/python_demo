#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: Section.py
@date: 2020/12/29 14:47
@desc: 
"""
L = ['张三', '李四', '王二', '刘五', '项项', '陈陈']
# 取出前三个元素原始方法
r = []
n = 3
for i in range(n):
    r.append(L[i])
print(r)
# 切片的方式取数据
print(L[0:3])
# 切片方式，从索引1开始，取出2个元素
print(L[1:3])
# 取出倒数第一个元素
print(L[-1])
# 取出倒数第二个开始的所有元素
print(L[-2:])

L = list(range(100))
print(L)
# 取出前10个数
print(L[:10])
# 取出后10个数
print(L[-10:])
# 前11-20个数
print(L[10:20])
# 前10个数，每两个取一个：
print(L[:10:2])
# 所有数,每5个取一个
print(L[::5])
print((0, 1, 2, 3, 4, 5)[:3])
# 字符串'xxx'也可以看成是一种list，每个元素就是一个字符
print('ABCDEFG'[:3])


def trim(s):
    if s[:1] != ' ' and s[-1:] != ' ':
        return s
    elif s[:1] == ' ':
        return trim(s[1:])
    else:
        return trim(s[:-1])


def trim1(s):
    while s[:1] == ' ':
        s = s[1:]
    while s[-1:] == ' ':
        s = s[0:-1]
    return s


if trim('hello  ') != 'hello':
    print('测试失败!')
elif trim('  hello') != 'hello':
    print('测试失败!')
elif trim('  hello  ') != 'hello':
    print('测试失败!')
elif trim('  hello  world  ') != 'hello  world':
    print('测试失败!')
elif trim('') != '':
    print('测试失败!')
elif trim('    ') != '':
    print('测试失败!')
else:
    print('测试成功!')

if trim1('hello  ') != 'hello':
    print('测试失败!')
elif trim1('  hello') != 'hello':
    print('测试失败!')
elif trim1('  hello  ') != 'hello':
    print('测试失败!')
elif trim1('  hello  world  ') != 'hello  world':
    print('测试失败!')
elif trim1('') != '':
    print('测试失败!')
elif trim1('    ') != '':
    print('测试失败!')
else:
    print('测试成功!')
