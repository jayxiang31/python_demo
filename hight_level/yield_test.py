"""
@author: 码农飞哥
@file: yield_test.py
@date: 2021/10/11 17:21
@desc: 
"""


def generate():
    a = 2
    while True:
        a += 1
        yield a

b = generate()
print(b)
print(next(b))
print(next(b))
print(next(b))