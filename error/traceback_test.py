# -*- utf-8 -*-
# @Time: 2021/8/1 18:52
# @Author: 码农飞哥
# @File: traceback_test.py
# @Software: PyCharm Community Edition

import traceback


class MyException(Exception):
    pass


def thirdMethod():
    raise MyException('自定义异常信息')


def secondMethod():
    thirdMethod()


def firstMethod():
    secondMethod()


def main():
    firstMethod()


try:
    main()
except:
    # 捕获异常，并将异常传播信息输出控制台
    traceback.print_exc()
    # 捕获异常，并将异常传播信息输出制定文件中
    traceback.print_exc(file=open('log.txt', 'a'))
