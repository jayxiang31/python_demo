"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: BusinessError.py
@date: 2021/8/1 19:25
@desc: 
"""


class BusinessError(Exception):
    # 自定义异常类型的初始化
    def __init__(self, msg):
        self.msg = msg
        # 返回异常类对象的说明信息
    def __str__(self):
        return ("{} 打印出来".format(repr(self.msg)))


try:
    raise BusinessError("业务出现异常了")
except BusinessError as e:
    print("错误是{}".format(e))
