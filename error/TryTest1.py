#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: TryTest.py
@date: 2020/12/30 19:25
@desc: 
"""
try:
    print('try....')
    r = 10 / 0
    print('result', r)
except ZeroDivisionError as e:
    print('', e.args)
    print('', str(e))
    print('', repr(e))
