#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: TryTest.py
@date: 2020/12/30 19:25
@desc: 
"""
try:
    print('try....')
    r = 10 / 0
    print('result', r)
except Exception as e:
    print('Exception', e)
except ZeroDivisionError as e:
    print('ZeroDivisionError', e)
finally:
    print('发生异常时finally语句块执行...')
print('END')


try:
    print('try....')
    r = 10 / 1
    print('result', r)
except Exception as e:
    print('Exception', e)
except ZeroDivisionError as e:
    print('ZeroDivisionError', e)
finally:
    print('没发生异常时finally语句块执行...')
print('END')

# SyntaxError
# IndentationError
# TypeError
# NameError
# AttributeError
# IndexError
# KeyError
