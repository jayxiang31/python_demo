"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: except_test.py
@date: 2021/8/1 19:40
@desc: 
"""
my_list = ['hello', 'python', 'java']
try:
    i = 0
    while True:
        print(my_list[i])
        i = i + 1
except:
    pass

i = 0
while i < len(my_list):
    print(my_list[i])
    i = i + 1
