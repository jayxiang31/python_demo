"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: raise_test.py
@date: 2021/8/1 21:40
@desc: 
"""
try:
    a = input("输入一个数：")
    # 判断用户输入的是否为数字
    if (not a.isdigit()):
        raise ValueError("a 必须是数字")
except ValueError as e:
    print("引发异常：", repr(e))

