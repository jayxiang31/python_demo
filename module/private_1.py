#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: private_1.py
@date: 2020/12/30 15:30
@desc: 
"""


def _private_1(name):
    return 'Hello,%s' % name


def _private_2(name):
    return 'Hi,%s' % name


def greeting(name):
    if len(name) > 3:
        return _private_1(name)
    else:
        return _private_2(name)
