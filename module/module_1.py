#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: module_1.py
@date: 2020/12/30 15:09
@desc: 
"""
# 引入sys模块
__author__ = '码农飞哥'

import sys
import os
import sys,os


def test():
    args = sys.argv
    if len(args) == 1:
        print('Hello,world!')
    elif len(args) == 2:
        print('Hello,%s!' % args[1])
    else:
        print('Too many arguments!')


if __name__ == '__main__':
    test()
