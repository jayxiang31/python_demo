"""
@author: 码农飞哥
@file: image_test2.py
@date: 2021/10/9 11:15
@desc: 
"""
from PIL import Image
from io import BytesIO
import requests

# 创建新图片
# res = requests.get(
#     'https://img-blog.csdnimg.cn/f2e98e08d5ec4283b08972c5ee8e1689.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA56CB5Yac6aOe5ZOl,size_20,color_FFFFFF,t_70,g_se,x_16').content
# # 从网页中读取图片,
# im2 = Image.open(BytesIO(res))
# b = BytesIO()
# im2.save(b, format='PNG')
# im2.show()

# 两张图片相加
im1 = Image.new('RGBA', (400, 400), 'red')
im2 = Image.new('RGBA', (400, 400), 'blue')
# 两张图片要一样大
Image.blend(im1, im2, 0.5)


# 通道分离：分割成三个通道，此时r,g,b分别为三个图像对象
r, g, b, a = im1.split()
print(r, g, b, a)
# 通道合并,将b,r两个通道进行翻转
img3 = Image.merge('RGB', (b, g, r))

# (left, upper, right, lower)
box = (0, 0, 100, 100)
region = im2.crop(box)  # 设置要裁剪的区域
region.show()

# 图片粘贴(合并）
im1.paste(region)  # 粘贴box大小的region到原先的图片对象中。(见图5)
im1.show()

# # 获取某个像素位置的值
# print('像素位置的值是', im1.getpixel((43, 23)))
#
# # 改变图片大小
img4 = im2.resize((300, 200))
img4.show()
#
# # 创建图像缩略图
# im1.thumbnail((40, 50))  # 图片大小为（10,20）
# im1.show()
#
# # 图片模式转换
# img3.convert('RGBA')
#
# # modes       Description
# #  1      1位像素，黑白图像，存成8位像素
# #  L      8位像素，黑白
# #  P      9位像素，使用调色板映射到任何其他模式
# # RGB     3*8位像素，真彩
# # RGBA    4*8位像素，真彩+透明通道
# # CMYK    4*8位像素，印刷四色模式或彩色印刷模式
# # YCbCr   3*8位像素，色彩视频格式
# #  I      32位整型像素
# #  F      33位浮点型像素
