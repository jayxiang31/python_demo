"""
@author: 码农飞哥
@file: image_draw_test3.py.py
@date: 2021/10/9 16:20
@desc: 
"""
from PIL import Image, ImageDraw

# 画点
im = Image.new('RGB', (600, 600), 'white')
draw = ImageDraw.Draw(im)

# # 创建一个正方形，[x1,x2,y1,y2] 或者[(x1,x2),(y1,y2)],fill 代表的为颜色
# draw.line([100, 100, 100, 500], fill='blue', width=2)  # 左竖线
# draw.line([100, 100, 500, 100], fill='blue', width=2)  # 上横线
# draw.line([500, 100, 500, 500], 'blue', width=2) #右竖线
# draw.line([100, 500, 500, 500], 'blue', width=2) #下横线
# im.save('picture/sequare.png', 'png')
# im.show()

# 弧形 [x1,x2,y1,y2] 弧度  颜色
draw.arc([100, 100, 600, 600], 0, 360, fill='black')
draw.arc([200, 100, 500, 600], 0, 360, fill='yellow')
im.show()

