"""
@author: 码农飞哥
@file: ImageColor_test.py
@date: 2021/9/29 17:00
@desc: 
"""
from PIL import ImageColor

# 获取颜色的RBGA值
rgb_tup = ImageColor.getrgb("#ff0000cc")
print(rgb_tup)

color_tup = ImageColor.getcolor("#ff0000cc", mode='RGBA')
print(color_tup)
