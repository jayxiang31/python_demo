"""
@author: 码农飞哥
@file: image_test.py
@date: 2021/9/29 14:56
@desc: 
"""
from PIL import Image
import os.path as op

base_path = "picture"

try:
    # 打开图片，返回Image对象
    img = Image.open(op.join(base_path, "img1.jpeg"))

    # 获取图片的宽高
    width, height = img.size
    print('图片的宽{0}，高{1}，'.format(width, height))
    # 展示图片
    img.show()
    # 旋转45度
    img.rotate(45).show()
    # 图片缩放
    img.thumbnail((width / 2, height / 2))
    # 保存图片
    img.save(op.join(base_path, 'thumbnail.jpeg'))

    # 创建新Images
    newImg = Image.new('RGB', (500, 500), (255, 0, 0))
    newImg.save(op.join(base_path, 'newImg.png'))
    # 复制图片
    copyImg = newImg.copy()
    newImg.save(op.join(base_path, 'copyImg.png'))
finally:
    # 这种打开方式需要手动关闭文件流
    img.close()

with Image.open(op.join(base_path, 'img1.jpeg')) as img:
    # 获取图片的宽高
    width, height = img.size
    print('图片的宽{0}，高{1}，'.format(width, height))

# 将两张图贴起来
img2 = Image.open(op.join(base_path, 'img2.jpeg'))
img3 = Image.open(op.join(base_path, 'img3.png'))
img2.paste(img3)
img2.save(op.join(base_path, 'beautiful_paste.jpeg'), 'jpeg')

# 透明底
img2 = Image.open(op.join(base_path, 'img2.jpeg')).convert('RGBA')
img3 = Image.open(op.join(base_path, 'img3.png')).convert('RGBA')
# 获取r,g,b,a的值
r, g, b, a = img3.split()
# 传入透明值
img2.paste(img3, box=(0, 0), mask=a)
img2.save(op.join(base_path, 'beautiful_paste2.png'))
