"""
@author: 码农飞哥
@file: cv_test.py
@date: 2021/10/8 11:27
@desc: 
"""
import cv2
img = cv2.imread("picture/weichat_img.jpeg", 1)
#param2为压缩参数，50代表压缩比例，该值范围在0-9，越小代表压缩力度越小
cv2.imwrite("picture/weichat_img_temp.jpeg", img, [cv2.IMWRITE_PNG_COMPRESSION, 6])
