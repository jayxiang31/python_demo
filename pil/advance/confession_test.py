"""
@author: 码农飞哥
@file: confession_test.py
@date: 2021/9/30 14:54
@desc: 表白应用
"""
# 3. 不断循环"我爱你"
from PIL import Image, ImageDraw, ImageFont

font_size = 8
text = "泳池美女"

img_raw = Image.open("../picture/beautiful.jpeg")
# 1.读取图像，获取每一个像素值
img_array = img_raw.load()
# 2. 新建画布，选好要使用的字体大小
new_img = Image.new('RGB', img_raw.size, (0, 0, 0))
font = ImageFont.truetype("../picture/simsun.ttf", size=font_size)
draw = ImageDraw.Draw(new_img)


# 搞一个生成器
def character_generate(text):
    while True:
        for i in range(len(text)):
            yield text[i]


char_gen = character_generate(text)
# 实现效果
for y in range(0, img_raw.size[1], font_size):
    for x in range(0, img_raw.size[0], font_size):
        draw.text((x+1, y+1), next(char_gen), font=font, fill=img_array[x, y])

new_img.convert('RGB').save("../picture/beautiful_result.jpeg")
