"""
@author: 码农飞哥
@file: image_test_temp.py
@date: 2021/10/8 08:50
@desc: 
"""
from PIL import Image, ImageEnhance

test_img = Image.open("../picture/weichat_img.jpeg")
print(test_img.size)
# test_img.thumbnail((3024, 3024), Image.ANTIALIAS)
cl = ImageEnhance.Color(test_img)
ce = cl.enhance(1.1)  # 对选择的属性数值增强1.2倍
ce.convert('L')
ce.save('../picture/thumbnail_weichat_img.jpeg', 'jpeg', quality=70, optimize=True, progressive=True)
# test_img.convert('L')
# test_img.save('../picture/thumbnail_weichat_img.jpeg', 'jpeg', quality=95, optimize=True, progressive=True)