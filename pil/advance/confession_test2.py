"""
@author: 码农飞哥
@file: confession_test2.py
@date: 2021/10/9 15:01
@desc: 添加水印
"""
from PIL import Image, ImageDraw, ImageFont

# 定义文本
font_size = 8
text = "美女"

img_raw = Image.open("../picture/beautiful.jpeg")
# 2. 新建画布，选好要使用的字体大小
new_img = Image.new('RGB', img_raw.size, (0, 0, 0))
font = ImageFont.truetype("../picture/simsun.ttf", size=font_size)
draw = ImageDraw.Draw(new_img)
width, height = img_raw.size

n = 0
# 实现效果
for y in range(0, height, font_size):
    for x in range(0, width, font_size):
        # 获取每个位置的像素
        a, b, c = img_raw.getpixel((x, y))  # 获取像素
        draw.text((x + 1, y + 1), (text[x % (len(text)):x % (len(text)) + 3]), font=font, fill=(a, b, c))
        n += 3

new_img.convert('RGB').save("../picture/beautiful_result2.jpeg")
