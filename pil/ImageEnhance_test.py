"""
@author: 码农飞哥
@file: ImageEnhance_test.py
@date: 2021/10/9 10:15
@desc: 用于设置图片的颜色对比度亮度锐度等啥的，增强图像。
"""
from PIL import ImageEnhance, Image

org_img = Image.open('picture/img10.png')
org_img.show(title='原图')

org_img
# 调整图像的颜色平衡
cl = ImageEnhance.Color(org_img)
# 对选择的属性数组增强1.2倍
ce = cl.enhance(1.2)
ce.show(title='颜色增强1.2倍')

# 调整图像的对比度
ct = ImageEnhance.Contrast(org_img)
ch = ct.enhance(3.4)
ch.show(title='对比度3.4')

# 调整图像的亮度
br = ImageEnhance.Brightness(org_img)
be = br.enhance(3)
be.show(title='亮度3')

# 调整图像的锐度
sp = ImageEnhance.Sharpness(org_img)
se = sp.enhance(200)
se.show(title='锐度200')
