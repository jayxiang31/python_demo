"""
@author: 码农飞哥
@file: imageFont_test.py
@date: 2021/9/29 17:39
@desc: 
"""
from PIL import ImageFont

img_font = ImageFont.truetype('picture/simsun.ttf', size=20)

hello = "hello"
print(img_font.getlength(hello))