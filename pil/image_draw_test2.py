"""
@author: 码农飞哥
@file: image_draw_test2.py
@date: 2021/10/9 11:39
@desc: 
"""
from PIL import Image, ImageDraw

# 画点
im = Image.new('RGB',(800,800),'white')
draw = ImageDraw.Draw(im)
# 指定点的坐标和颜色
draw.point([700,700], fill='blue')
draw.point([701,701], fill='blue')


# # 创建一个正方形，[x1,x2,y1,y2] 或者[(x1,x2),(y1,y2)],fill 代表的为颜色
draw.line([100, 100, 100, 600], fill='blue',width=2)  # 直线
draw.line([100, 100, 600, 100], fill='blue',width=2)
draw.line([600, 100, 600, 600], 'blue',width=2)
draw.line([100, 600, 600, 600], 'blue',width=2)

# 弧形 [x1,x2,y1,y2] 弧度  颜色
draw.arc([100, 100, 600, 600], 0, 360, fill='black')
draw.arc([200, 100, 500, 600], 0, 360, fill='yellow')

# 画图 [x1,x2,y1,y2] outline边框颜色，fill 填充颜色
draw.ellipse([100, 100, 600, 600], outline='black', fill='white')

# 画半圆[x1,x2,y1,y2] 弧度   outline:弦线颜色  fill 填充颜色
draw.chord([100, 100, 600, 600], 0, 360, outline=125)
draw.chord([100, 100, 600, 600], 0, 90, outline=158)
draw.chord([100, 100, 600, 600], 90, 180, outline=99, fill='gray')

# 扇形[x1,x2,y1,y2] 弧度 outline 弦线颜色 fill 填充颜色
draw.pieslice([100, 100, 600, 600], 180, 210, outline=255)
draw.pieslice([100, 100, 600, 600], 30, 80, fill=255)

# 多边形
draw.polygon([10, 23, 45, 6, 77, 87], outline='orange')
draw.polygon([10, 20, 30, 40, 50, 90, 70, 80, 90, 100], fill='red')

# 矩形
draw.rectangle((200, 200, 500, 500), outline='yellow')
draw.rectangle((250, 300, 450, 400), fill=128)
im.show()
