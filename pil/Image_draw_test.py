"""
@author: 码农飞哥
@file: Image_draw_test.py
@date: 2021/9/29 17:13
@desc: 
"""

# 画交叉线
from PIL import ImageDraw, ImageFont, ImageColor
from PIL import Image
import os.path as op

base_path = "picture"

# 在原始图片上画线
with Image.open(op.join(base_path, "img4.jpeg")) as im:
    draw = ImageDraw.Draw(im)
    draw.line((0, 0) + im.size, fill=128)
    draw.line((0, im.size[1], im.size[0], 0), fill=128)
    im.save(op.join(base_path, "img4_line.jpeg"), "jpeg")

# 给图片上添加文字
with Image.open(op.join(base_path, 'img4.jpeg')) as im:
    font = ImageFont.truetype(op.join(base_path, 'simsun.ttf'), size=80)
    rgb_tup = ImageColor.getrgb("#ff0000cc")
    draw = ImageDraw.Draw(im)
    text = "玛莎拉蒂"
    draw.text((650, 550), text, fill=rgb_tup, font=font)
    im.save(op.join(base_path, '玛莎拉蒂.png'), 'png')

