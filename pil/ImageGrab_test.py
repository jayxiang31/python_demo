"""
@author: 码农飞哥
@file: ImageGrab_test.py
@date: 2021/10/9 10:15
@desc: 
"""
from PIL import ImageGrab
im1=ImageGrab.grab((0,0,800,300)) #截取屏幕指定区域的图像
im2=ImageGrab.grab() #不带参数表示全屏幕截图
im1.show()
im2.show()
