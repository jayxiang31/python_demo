#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: str_test.py
@date: 2020/12/28 14:28
@desc: 
"""
import os

print('Hi,%s,you have $%d.' % ('小项', 10000))
print('growth rate:%d %%' % 7)
# 小明的成绩从去年的72分提升到了今年的85分，
# 请计算小明成绩提升的百分点，并用字符串格式化显示出'xx.x%'，只保留小数点后1位：
r = (85 / 72 - 1) * 100
print('{r:.1f}')

path = "test_user_info.py"
# 第一种
# os.path.splittext(path)[-1]
# 第二种
bool = path.endswith(".py")
print(bool)
# 第三种
path = "test_user_info.py"
if ".py" in path:
    print(True)
# 第四种
suffix = path.split(".")[1]
print("suffix: {}".format(suffix))


