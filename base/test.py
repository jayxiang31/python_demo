"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: test.py
@date: 2021/8/22 23:07
@desc: 
"""
str6 = '程序猿正在创造世界'
# 索引的坐标是从0开始的，3表示取第4个字符串，6表示到第七个字符串（不包括），步长是2
print(str6[3:6:2])
# 首先获取反转之后的字符串
str2 = ''.join(reversed(str6))
# 将字符串拼接成我们想要的字符
print(str2[0:4] + '的默默' + str2[6:9])

str6 = '程序猿正在创造世界'
# 第一题
print(str6[3]+str6[5])
# 第二题
str6=str6[0:3]+str6[5:]
list2 = list(reversed(str6))
list2.insert(4, '的默默')
print(''.join(list2))


str3 = '码农飞哥'
print('字符串的长度=', len(str3))
print('转成列表=', list(str3))
print('调用enumerate函数', enumerate(str3))
print('遍历enumerate函数的结果:')
for item in enumerate(str3):
    print(item)

print('遍历reversed函数的结果:')
for item in reversed(str3):
    print(item)

list2 = ['码农', '飞哥']
print('列表转字符串=', str(list2))
list1 = [12, 20, 5, 8, 1]
print('最大值=', max(list1))
print('最小值=', min(list1))
print('求和结果=', sum(list1))
print('排序结果=', sorted(list1))

str6 = '码农飞哥好，'
# 使用+ 运算符号
print('+运算符拼接的结果=', (str6 + '码农飞哥牛逼'))
# 使用join拼接字符串
list2 = ['码', '农', '飞', '哥', '牛', '逼']
print('无符号分割join的拼接结果=', ''.join(list2))
print('逗号分割join的拼接结果=', ','.join(list2))
# 使用format拼接
str7 = str6 + '{0}'
print('format拼接的结果=', str7.format('码农飞哥牛逼'))
str8 = str6 + '{0}{1}'
print('format拼接的结果=', str8.format('码农飞哥', '牛逼'))
