#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: dict_test.py
@date: 2020/12/28 15:52
@desc: 
"""
# 创建字典
d = {'name': '码农飞哥', 'age': 18, 'height': 185}
print(d)
list = ['name', 'age', 'height']
dict_demo = dict.fromkeys(list)
dict_demo1 = dict.fromkeys(list, '测试')
print(dict_demo)
print(dict_demo1)
# 通过dict()映射创建字典,传入列表或者元组
demo = [('name', '码农飞哥'), ('age', 19)]
dict_demo2 = dict(demo)
print(dict_demo2)
dict_demo21 = dict(name='码农飞哥', age=17, weight=63)
print(dict_demo21)

keys = ['name', 'age']
values = ['码农飞哥', 20]
dict_demo3 = dict(zip(keys, values))
print(dict_demo3)

# 访问字典
dict_demo5 = {'name': '码农飞哥', 'age': 18, 'height': 185}
print(dict_demo5['name'])
print(dict_demo5.get('name'))
print('键不存在的情况返回结果=', dict_demo5.get('test'))

# 添加键值对
dict_demo6 = {'name': '码农飞哥', 'age': 18, 'height': 185}
dict_demo6['sex'] = '男'
print('新增键值对的结果={0}'.format(dict_demo6))
# 修改键值对
dict_demo6['height'] = 190
print('修改键值对的结果={0}'.format(dict_demo6))

dict_demo10 = {'name': '码农飞哥', 'age': 18, 'height': 185, 'width': 100}
# 删除键值对
del dict_demo6['height']
print('删除键height对之后的结果=', dict_demo6)
# pop()方法和popitem()方法
dict_demo10.pop('width')
print('pop方法调用删除键width之后结果=', dict_demo10)
dict_demo10 = {'name': '码农飞哥', 'age': 18, 'height': 185, 'width': 100}
dict_demo10.popitem()
print('popitem方法调用之后结果=', dict_demo10)
# 判断键是否存在
print('判断键name是否存在的结果是=', ('name' in dict_demo6))

# 查询dict包含了哪些方法
print('dict包含的方法有={0}'.format(dir(dict)))
dict_demo7 = {'name': '码农飞哥', 'age': 18, 'height': 185, 'width': 100}
print('keys方法返回结果=', dict_demo7.keys())
print('value方法返回结果=', dict_demo7.values())
print('item方法返回结果=', dict_demo7.items())

dict_demo8 = dict_demo7.copy()
print('copy方法返回结果=', dict_demo8)
# update方法
dict_demo7 = {'name': '码农飞哥', 'age': 18, 'height': 185, 'width': 100}
dict_demo7.update({'name': '飞飞1024', 'age': 25, 'like': '学习'})
print('update方法返回结果={}', dict_demo7)

str = '码农飞哥{0},你一定会有={1}'
print(str.format('加油', '出息'))
str = '码农飞哥的第=%(num)s篇=%(article)s,希望=%(reader)s喜欢'
dict_demo12 = {'num': 250, 'article': '文章', 'reader': '读者们'}
print(str % dict_demo12)


d = {None: '码农飞哥', 'age': 18, 'height': 185}
print(d)
