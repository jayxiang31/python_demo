str = 'https://feige.blog.csdn.net/'
print('不指定分割次数', str.split('.'))
print('指定分割次数为2次', str.split('.', 2))

list2 = ['码农飞哥', '好好学习', '非常棒']
print('通过.来拼接', '.'.join(list2))
print('通过-来拼接', '-'.join(list2))

str = 'https://feige.blog.csdn.net/'
print('统计.出现的次数', str.count('.'))
print('从1位置到倒数第五个位置统计.出现的次数', str.count('.', 1, -5))

str = '码农飞哥'
print('检索是否包含字符串"飞哥"', str.find('飞哥'))
print("检索是否包含字符串'你好'", str.find('你好'))

str1 = 'https://feige.blog.csdn.net/'
str2 = 'https://www.baidu.com/'
print("通过-实现左对齐", str1.ljust(30, '-'))
print("通过-实现左对齐", str2.ljust(30, '-'))
print("通过-实现右对齐", str1.rjust(30, '-'))
print("通过-实现右对齐", str2.rjust(30, '-'))
print("通过-实现居中对齐", str1.center(30, '-'))
print("通过-实现居中对齐", str2.center(30, '-'))

str1 = 'https://feige.blog.csdn.net/'
print('是否是以https开头', str1.startswith('https'))
print('是否是以feige开头', str1.startswith('feige', 0, 20))

str = 'feiGe勇敢飞'
print('首字母大写', str.title())
print('全部小写', str.lower())
print('全部大写', str.upper())

str2 = '\n码农飞哥勇敢飞 '
print('去除前后空格(特殊字符串)', str.strip())
print('去除左边空格(特殊字符串)', str.lstrip())
print('去除右边空格(特殊字符串)', str.rstrip())

str2 = '码农飞哥加油'
bytes = str2.encode()
print('编码', bytes)
print('解码', bytes.decode())

import json

dict = {'学号': 1001, 'name': "张三", 'score': [{'语文': 90, '数学': 100}]}
str2 = json.dumps(dict, ensure_ascii=False)
print('序列化成字符串', str2, type(str2))
dict2 = json.loads(str2)
print('反序列化成对象', dict2, type(dict2))


