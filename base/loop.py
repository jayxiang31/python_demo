#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: loop.py
@date: 2020/12/28 15:38
@desc: 
"""
names = ['Michael', 'Bob', 'Tracy']
for name in names:
    print(name)
sum = 0
for x in range(101):
    sum = sum + x
print(sum)
sum = 0
n = 99
while n > 0:
    sum = sum + n
    n = n - 2
print(sum)

L = ['Bart', 'Lisa', 'Adam']
for name in L:
    print('Hello,%s!' % (name))

n = 1
while n <= 100:
    if n > 10:
        break
    print(n)
    n = n + 1
print('END')

while True:
    print("你好")
