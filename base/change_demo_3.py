counter=100 #赋值整型变量
miles=1000.0 #浮点型
name="John" #字符串
print(counter)
print(miles)
print(name)

var1=1
var2=10

str="Hello World!"
print(str) #输出完整字符串
print(str[0]) #输出字符串中的第一个字符
print(str[2:5]) #输出字符串中的第三个至第六个之间的字符串
print(str[2:]) #输出从第三个字符串开始的字符串
print(str*2) #输出字符串两次
print(str+"TEST") #输出连接的字符串

# Python列表 List(列表)是Python中使用最频繁的数据类型
# 列表可以完成大多数集合类的数据结构实现，它支持字符，数字，字符串甚至可以包含列表。
# 列表用[]标识，是Python最通用的复合数据类型
list=['runoob',786,2.23,'john',70.2]
tinylist=[123,'john']
print(list)  #输出完整列表
print(list[0]) #输出列表的第一个元素
print(list[1:3]) #输出第二个至第三个元素
print(list[2:]) #输出从第三个开始至列表末尾的所有元素
print(tinylist*2)#输出列表两次
print(list+tinylist) #打印组合的列表
name=input()
print("输入的用户名是："+name)





