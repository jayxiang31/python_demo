#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: condition_test.py
@date: 2020/12/28 15:22
@desc: 
"""
age = 3
if age >= 18:
    print('your age is', age)
    print('adult')
elif age >= 6:
    print('teenager')
else:
    print("your age is", age)
    print('kid')

bmi = 80.5/(1.75*1.75)
if bmi<18.5:
    print('过轻')
elif 18.5<=bmi<25:
    print('正常')
elif 25<=bmi<28:
    print('过重')
elif 28<=bmi<32:
    print('肥胖')
else:
    print('严重肥胖')
pass

