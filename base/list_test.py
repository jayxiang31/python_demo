#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: list_test.py
@date: 2020/12/28 14:45
@desc:
"""
# 定义list
classmates = ['Michael', 'Bob', 'Tracy']
print(classmates)
# 获取list元素的个数
print(len(classmates))
# list是一个可变的有序表，所以，可以往list中追加元素到末尾
classmates.append('Adam')
print(classmates)
# 把元素插入到指定的位置，比如索引号为1的位置
classmates.insert(1, 'Jack')
print(classmates)
# 删除list末尾的元素
classmates.pop()
print(classmates)
# 删除指定位置的元素，用pop(i)方法，其中i是索引位置
classmates.pop(1)
print(classmates)
# 把某个元素替换成别的元素，可以直接赋值给对应的索引位置
classmates[1] = 'Sarah'
print(classmates)
L = []
print(len(L))
list_name = list('码农飞哥')


name_list = ['码农飞哥', '小伟', '小小伟', '超人']
print()


