if True:
    print("Answer")
    print("True")
else:
    print("Answer")
    print("False")

x = "a"
y = "b"
# 换行输出
print(x)
print(y)
print('---------')
# 不换行输出
print(x),
print(y),

# 不换行输出
print(x, y)
# 判断
a = -100
if a >= 0:
    print("输出正数" + str(a))
else:
    print("输出负数" + str(a))
# 浮点数
print(1.2e-5)
print(1.23)

# n = 123
# f = 456.789
# s1 = 'Hello, world'
# s2 = 'Hello, \'Adam\''
# s3 = r'Hello, "Bart"'
# s4 = r'''Hello,
# Lisa!'''
print(123)
print(456.789)
print('Hello, world')
print('Hello, \\\'Adam\\\'')
print('r\'Hello, "Bart"\'')
print('r\'\'\'Hello,')
print('Lisa!\'\'\'')