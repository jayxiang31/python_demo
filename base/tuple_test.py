#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: tuple.py
@date: 2020/12/28 15:10
@desc: 
"""
# t = ('a', 'b', ['A', 'B'])
# t[2][0]='X'
# t[2][1]='Y'
# print(t)

# tuple_name = ('码农飞哥', '小伟', '小小伟', '超人')
# print(tuple_name)
# tuple2 = '码农飞哥', '小伟', '小小伟', '超人'
# print(type(tuple2))

# name_list = ['码农飞哥', '小伟', '小小伟', '超人']
# print(tuple(name_list))
# print(tuple('码农飞哥'))
# tuple_name = ('码农飞哥', '小伟', '小小伟', '超人')
# # 获取索引为1的元素值
# print(tuple_name[1])
# #获取索引为1到索引为2之间的元素值，不包括索引2本身
# print(tuple_name[0:2])
# tuple_name[1]='张三'


listDemo = []
print(listDemo.__sizeof__())
tupleDemo = ()
print(tupleDemo.__sizeof__())
