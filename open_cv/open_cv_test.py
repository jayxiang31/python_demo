# # -*- utf-8 -*-
# """
# @url: https://blog.csdn.net/u014534808
# @Author: 码农飞哥
# @File: open_cv_test.py
# @Time: 2021/11/21 13:09
# @Desc:
# """
# # coding=utf-8
# # 图片修复
#
# import cv2
# import numpy as np
#
# path = "doctor.jpg"
#
# img = cv2.imread(path)
# hight, width, depth = img.shape[0:3]
#
# # 图片二值化处理，把[240, 240, 240]~[255, 255, 255]以外的颜色变成0
# thresh = cv2.inRange(img, np.array([240, 240, 240]), np.array([255, 255, 255]))
#
# # 创建形状和尺寸的结构元素
# kernel = np.ones((3, 3), np.uint8)
#
# # 扩张待修复区域
# hi_mask = cv2.dilate(thresh, kernel, iterations=1)
# specular = cv2.inpaint(img, hi_mask, 5, flags=cv2.INPAINT_TELEA)
#
# cv2.namedWindow("Image", 0)
# cv2.resizeWindow("Image", int(width / 2), int(hight / 2))
# cv2.imshow("Image", img)
#
# cv2.namedWindow("newImage", 0)
# cv2.resizeWindow("newImage", int(width / 2), int(hight / 2))
# cv2.imshow("newImage", specular)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# 方式一
import cv2

path = "doctor.jpg"


def get_water():
    # 黑底白字
    src = cv2.imread(path)  # 默认的彩色图(IMREAD_COLOR)方式读入原始图像
    # black.jpg
    mask = cv2.imread('di.png', cv2.IMREAD_GRAYSCALE)  # 灰度图(IMREAD_GRAYSCALE)方式读入水印蒙版图像
    # 参数：目标修复图像; 蒙版图（定位修复区域）; 选取邻域半径; 修复算法(包括INPAINT_TELEA/INPAINT_NS， 前者算法效果较好)
    dst = cv2.inpaint(src, mask, 3, cv2.INPAINT_NS)

    cv2.imwrite('result1.jpg', dst)


get_water()
