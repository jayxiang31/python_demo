"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: thread_test2.py
@date: 2021/8/15 20:42
@desc: 
"""
import threading


class MyThread(threading.Thread):
    def __init__(self, add):
        # threading.Thread.__init__(self)
        super().__init__()
        self.add = add
        # 重写run()方法

    def run(self):
        for arc in self.add:
            print(threading.current_thread().getName() + " " + arc)


my_tuple = ("码农飞哥", "好好学习", "早日突破职业瓶颈")

thread = MyThread(my_tuple)
thread.start()

for i in range(4):
    print(threading.current_thread().getName() + "执行" + str(i) + "次")
