"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: sleep_thread_test.py
@date: 2021/8/15 21:34
@desc: 
"""
import threading
import time


# 定义线程要调用的方法
def async_fun(*add):
    for arc in add:
        start_time = time.time()
        time.sleep(2)
        print(str((time.time() - start_time)) + " 秒 " + threading.current_thread().getName() + " 结束调用" + arc)


my_tuple = ("码农飞哥", "好好学习", "早日突破职业瓶颈")

# 创建线程
thread = threading.Thread(target=async_fun, args=my_tuple)
# 启动线程
thread.start()
