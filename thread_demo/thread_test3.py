"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: thread_test3.py
@date: 2021/8/15 21:22
@desc: 
"""
import threading


# 定义线程要调用的方法
def async_fun(*add):
    for arc in add:
        print(threading.current_thread().getName() + " " + arc)


my_tuple = ("码农飞哥", "好好学习", "早日突破职业瓶颈")

# 创建线程
thread = threading.Thread(target=async_fun, args=my_tuple, name="线程1")
thread2 = threading.Thread(target=async_fun, args=my_tuple, name='线程2')
# 启动线程
thread.start()
# 线程1执行完之后启动线程2
thread2.start()
thread2.join()
