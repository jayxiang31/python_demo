"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: thread_test.py
@date: 2021/8/15 20:35
@desc: 
"""
import threading

# 定义线程要调用的方法
def async_fun(*add):
    for arc in add:
        print(threading.current_thread().getName() + " " + arc)


my_tuple = ("码农飞哥", "好好学习", "早日突破职业瓶颈")

# 创建线程
thread = threading.Thread(target=async_fun, args=my_tuple)
# 启动线程
thread.start()

for i in range(4):
    print(threading.current_thread().getName() + "执行" + str(i)+ "次")
