# """
# @author: 码农飞哥
# @contact: https://feige.blog.csdn.net/
# @file: dead_lock.py
# @date: 2021/8/24 08:20
# @desc: 死锁最典型的例子就是哲学家问题，哲学家问题五个哲学家
# """
# import threading
# import time
#
#
# class DeadLockTest:
#
#
# # 筷子
# class ChopsticksLeft(object):
#     def __init__(self):
#         self.lock = threading.RLock()
#
#
# class ChopsticksRight:
#     def __init__(self):
#         self.lock = threading.RLock()
#
#
# class Philosopher(threading.Thread):
#     def __init__(self, left, right):
#         threading.Thread.__init__(self)
#         self.left = left
#         self.right = right
#
#     def run(self):
#         # 思考1秒
#         time.sleep(1)
#         self.left
