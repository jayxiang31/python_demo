"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: lock_test.py
@date: 2021/8/24 07:49
@desc: 
"""
import threading


class Account:
    # 定义构造函数
    def __init__(self, account_no, balance):
        """
        :param account_no:  账户
        :param balance:  余额
        """
        self.account_no = account_no
        self._balance = balance

    def draw(self, draw_amount):
        """
        :param draw_amount: 需要取的钱
        :return:
        """
        if self._balance > draw_amount:
            print(threading.current_thread().getName() + '从' + self.account_no + " 取钱成功，账户取出金额是:" + str(
                draw_amount) + "\n")
            self._balance = self._balance - draw_amount
            print('账户余额是', self._balance)
        else:
            print(threading.current_thread().getName() + '从' + self.account_no + " 取钱失败\n")


# 两个线程并发取钱
account = Account('账户一', 2000)
threading.Thread(target=account.draw, name='线程一', args=(800,)).start()
threading.Thread(target=account.draw, name='线程二', args=(800,)).start()
threading.Thread(target=account.draw, name='线程三', args=(800,)).start()
