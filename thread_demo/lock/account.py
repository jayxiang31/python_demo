#!/usr/bin/env python
# coding=utf-8

"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: account.py
@date: 2021/8/26 20:38
@desc: 
"""
import threading


class Account:
    # 定义构造函数
    def __init__(self, account_no, balance):
        """
        :param account_no:  账户
        :param balance:  余额
        """
        self.account_no = account_no
        self._balance = balance
        self.condition = threading.Condition()
        # 定义代表是否已经存钱的标识
        self.__deposit_flag = False

    # 取钱
    def draw(self, draw_amount):
        # 加锁
        self.condition.acquire()
        try:
            # 还没存钱
            if not self.__deposit_flag:
                self.condition.wait()
            else:
                if self._balance >= draw_amount:
                    self._balance = self._balance - draw_amount
                    print(threading.current_thread().getName() + " 取钱成功，账户余额是:" + str(self._balance) + "\n")
                else:
                    print(threading.current_thread().getName() + " 取钱失败\n")
                # 将标识账户已有存款的标识改成False
                self.__deposit_flag = False
                # 唤醒其他等待现车线程
                self.condition.notify_all()
        finally:
            # 释放锁
            self.condition.release()

    # 存钱
    def deposit(self, deposit_amount):
        # 加锁
        self.condition.acquire(timeout=12)
        try:
            # 如果已经存款了，则等待取款
            if self.__deposit_flag:
                self.condition.wait()
            else:
                self._balance = self._balance + deposit_amount
                print(threading.current_thread().getName() + " 存款成功，存款金额是:" + str(deposit_amount) + "\n")
                # 将存款标识改成已存款
                self.__deposit_flag = True
                # 唤醒其他线程
                self.condition.notify_all()
        finally:
            # 释放锁
            self.condition.release()


def draw_many(account, draw_amount, max):
    for i in range(max):
        account.draw(draw_amount)


def deposit_many(account, deposit_amount, max):
    for i in range(max):
        account.deposit(deposit_amount)


# 创建一个账户
account = Account("账户一", 0)
# 创建并启动取钱线程
draw_1 = threading.Thread(name='取钱者一', target=draw_many, args=(account, 200, 50))
draw_1.start()
draw_2 = threading.Thread(name='取钱者二', target=draw_many, args=(account, 200, 50))
draw_2.start()
# 创建
deposit_1 = threading.Thread(name='存钱者一', target=deposit_many, args=(account, 200, 50))
deposit_1.start()
deposit_2 = threading.Thread(name='存钱者二', target=deposit_many, args=(account, 200, 50))
deposit_2.start()
draw_1.join()
draw_2.join()
deposit_1.join()
deposit_2.join()
