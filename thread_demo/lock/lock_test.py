#-*- utf-8 -*-
"""
@url: https://blog.csdn.net/u014534808
@Author: 码农飞哥
@File: lock_test.py
@Time: 2021/12/3 08:52
@Desc:
"""
import threading
import time

# 全局变量
gl_num = 0

# 创建互斥锁
suo = threading.Lock()


# 将该函数放入子线程1中执行，用于将全局变量循环自加1
def sumx(num):
    global gl_num

    for i in range(num):
        suo.acquire()  # 开启互斥锁
        gl_num += 1
        suo.release()  # 关闭互斥锁
    print("子线程1中sumx结果为：%d" % gl_num)


# 将该函数放入子线程2中执行，用于将全局变量循环自加1
def sumz(num):
    global gl_num

    for i in range(num):
        suo.acquire()  # 开启互斥锁
        gl_num += 1
        suo.release()  # 关闭互斥锁
    print("子线程2中sumz结果为：%d" % gl_num)


def sumy(num):
    global gl_num

    for i in range(num):
        suo.acquire()  # 开启互斥锁
        gl_num += 1
        suo.release()  # 关闭互斥锁
    print("子线程3中sumz结果为：%d" % gl_num)


def main():
    # 创建3个子线程并开始执行
    t1 = threading.Thread(target=sumx, args=(1000000,))
    t2 = threading.Thread(target=sumz, args=(1000000,))
    t3 = threading.Thread(target=sumy, args=(1000000,))

    t1.start()
    t2.start()
    t3.start()

    # 5秒钟后执行下面的语句
    time.sleep(5)
    print("主线程中结果为：%d" % gl_num)


if __name__ == "__main__":
    main()