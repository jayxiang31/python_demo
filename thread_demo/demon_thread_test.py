"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: demon_thread_test.py
@date: 2021/8/15 21:28
@desc: 
"""
import threading


# 定义线程要调用的方法
def async_fun(*add):
    for arc in add:
        print(threading.current_thread().getName() + " " + arc + "\n")


my_tuple = ("码农飞哥", "好好学习", "早日突破职业瓶颈")

# 创建线程
thread = threading.Thread(target=async_fun, args=my_tuple)
# 设置未守护线程
thread.daemon=True
# 启动线程
thread.start()


for i in range(2):
    print(threading.current_thread().getName() + "执行" + str(i) + "次" + "\n")
