"""
@author: 码农飞哥
@file: timer.py
@date: 2021/9/8 22:07
@desc: 
"""
from threading import Timer
import threading


def say_hello():
    print(threading.current_thread().name + '码农飞哥，你好')


t = Timer(2, say_hello)
t.start()
print(threading.current_thread().name + '主线程执行完')
