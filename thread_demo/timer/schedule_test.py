"""
@author: 码农飞哥
@file: schedule_test.py
@date: 2021/9/8 22:13
@desc: 
"""

import threading
from sched import scheduler


def action(arg):
    print(threading.current_thread().name + ' ' + arg)


def thread_action(*add):
    # 创建任务调度对象
    sche = scheduler()
    # 定义优先级
    i = 1

    for arc in add:
        # 指定1秒后执行action函数
        sche.enter(1, i, action, argument=(arc,))
        i = i + 1
    # 执行所有调度的任务
    sche.run()


# 定义为线程方法传入的参数
my_tuple = ("等级1的任务", '等级2的任务', '等级3的任务')
thread = threading.Thread(target=thread_action, args=my_tuple)
thread.start()
