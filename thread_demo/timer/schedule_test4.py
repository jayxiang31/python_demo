"""
@author: 码农飞哥
@file: schedule_test2.py
@date: 2021/9/8 22:38
@desc: 
"""
import schedule
import time
import threading


def job1():
    print("任务1开始执行\n")
    time.sleep(1)
    print("任务1执行完成\n")


def job2():
    print("任务2开始执行\n")
    time.sleep(1)
    print("任务2执行完成\n")


def thread_job(fun):
    threading.Thread(target=fun).start()


# 每隔1分钟执行一次任务
schedule.every(1).seconds.do(thread_job, job1)
schedule.every(1).seconds.do(thread_job, job2)

while True:
    schedule.run_pending()
    time.sleep(1)
