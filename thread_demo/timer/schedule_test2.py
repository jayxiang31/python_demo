"""
@author: 码农飞哥
@file: schedule_test2.py
@date: 2021/9/8 22:38
@desc: 
"""
import schedule
import time


def job(name):
    print("他的名字是：", name)


name = "码农飞哥"
# 每隔1分钟执行一次任务
schedule.every(1).minutes.do(job, name)
# 每隔一小时执行一次任务
schedule.every().hour.do(job, name)
# 每天10:30执行一次任务
schedule.every().day.at("10:30").do(job, name)
# 每隔5到10天执行一次任务
schedule.every(5).to(10).days.do(job, name)
# 每周一执行一次任务
schedule.every().monday.do(job, name)
# 每周三13:15执行一次任务
schedule.every().wednesday.at("13:15").do(job, name)

while True:
    schedule.run_pending()
    time.sleep(1)
