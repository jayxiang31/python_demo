"""
@author: 码农飞哥
@file: thread_local_test1.py
@date: 2021/9/4 21:47
@desc: 
"""

import threading
import time

global_goods_dict = {}

# {
#     "线程ID":{"telephone":"放入的具体手机"},
#     "线程ID":{"telephone":"放入的具体手机"},
#     "线程ID":{"telephone":"放入的具体手机"}
#
# }

def set_telephone(telephone):
    # 获取线程ID
    thread_id = threading.get_ident()
    global_goods_dict[thread_id] = {}
    global_goods_dict[thread_id]["telephone"] = telephone
    print(threading.current_thread().name + " 放入的手机是", telephone)
    time.sleep(1)
    get_telephone()


def get_telephone():
    thread_id = threading.get_ident()
    print(threading.current_thread().name + " 取出的手机是", global_goods_dict[thread_id]["telephone"])


if __name__ == '__main__':
    for i in range(3):
        thread = threading.Thread(target=set_telephone, name='学生' + str(i), args=('手机' + str(i),))
        thread.start()
