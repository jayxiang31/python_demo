"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: executor_test2.py
@date: 2021/8/29 11:41
@desc: 
"""
from concurrent.futures import ThreadPoolExecutor
import threading


def async_add(max):
    sum = 0
    for i in range(max):
        sum = sum + i
        print(threading.current_thread().name + '执行求和操作 i=' + str(i) + "求得的和是=" + str(sum))
    return sum


with ThreadPoolExecutor(max_workers=2) as pool:
    # 向线程池提交一个task
    future1 = pool.submit(async_add, 20)
    future2 = pool.submit(async_add, 30)


    # 定义获取结果的函数
    def get_result(future):
        print(future.result())


    # 查看future1代表的任务返回的结果
    print('线程一的执行结果是=' + str(future1.add_done_callback(get_result)))
    # 查看future2代表的任务的返回结果
    print('线程一的执行结果是=' + str(future2.add_done_callback(get_result)))
    print('------------主线程执行结束----')
