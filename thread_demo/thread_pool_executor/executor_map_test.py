"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: executor_map_test.py
@date: 2021/8/29 11:59
@desc: 
"""
from concurrent.futures import ThreadPoolExecutor
import threading


def async_add(max):
    sum = 0
    for i in range(max):
        sum = sum + i
        print(threading.current_thread().name + '执行求和操作 i=' + str(i) + "求得的和是=" + str(sum))
    return sum


with ThreadPoolExecutor(max_workers=4) as pool:
    # 使用线程池执行max计算
    results = pool.map(async_add, (20, 30, 40))
    print('-----------')
    for i in results:
        print(i)
