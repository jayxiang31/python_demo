"""
@author: 码农飞哥
@contact: https://feige.blog.csdn.net/
@file: executor_test.py
@date: 2021/8/29 11:10
@desc: 
"""
from concurrent.futures import ThreadPoolExecutor
import threading
import time


def async_add(max):
    sum = 0
    for i in range(max):
        sum = sum + i
        print(threading.current_thread().name + '执行求和操作 i=' + str(i) + "求得的和是=" + str(sum))
    return sum


# 创建两个线程
pool = ThreadPoolExecutor(max_workers=2)
# 向线程池提交一个task,20作为async_add()函数的参数
future1 = pool.submit(async_add, 20)
# 向线程池再提交一个task
future2 = pool.submit(async_add, 30)
# 判断future1代表的任务是否执行完
print(future1.done())
time.sleep(1)
print(future2.done())
# 查看future1代表的任务返回的结果
print('线程一的执行结果是='+str(future1.result()))
# 查看future2代表的任务的返回结果
print('线程一的执行结果是='+str(future2.result()))
# 关闭线程池
pool.shutdown()
print("----"+threading.current_thread().name+"----主线程执行结束-----")