# -*- utf-8 -*-
"""
@url: https://blog.csdn.net/u014534808
@Author: 码农飞哥
@File: pool_test2.py
@Time: 2021/11/28 21:28
@Desc:
"""
from multiprocessing.pool import ThreadPool
import threading


def async_add(max):
    sum = 0
    for i in range(max):
        sum = sum + i
        print(threading.current_thread().name + '执行求和操作 i=' + str(i) + "求得的和是=" + str(sum))
    return sum


# 创建线程池,线程池的大小是3
pool = ThreadPool(processes=3)
for i in range(3):
    pool.apply_async(func=async_add, args=(100,))
print(threading.current_thread().name + '执行完成')
